// paralax
(function(e){var t=e(window);var n=t.height();t.on('resize',function(){n=t.height()});e.fn.parallax=function(r,i,s){function l(){o.each(function(){a=o.offset().top});if(s){u=function(e){return e.outerHeight(true)}}else{u=function(e){return e.height()}}if(arguments.length<1||r===null)r="50%";if(arguments.length<2||i===null)i=.5;if(arguments.length<3||s===null)s=true;var f=t.scrollTop();o.each(function(){var t=e(this);var s=t.offset().top;var l=u(t);if(s+l<f||s>f+n){return}o.css("backgroundPosition",r+" "+Math.round((a-f)*i)+"px")})}var o=e(this);var u;var a;var f=0;t.on('scroll',l).on('resize', l);l()}})(jQuery);

/**
 * jQuery Tabs plugin 1.0.0
 *
 * @author Theme Alien
 */
( function( $ ) {
	$.fn.tabs = function() {
		return this.each( function() {
			var $element = $( this ),
				$nav     = $element.find( '.tabs-nav' ),
				$tabs    = $nav.find( 'a' ),
				$panels  = $element.find( '.tabs-panel' );

			$tabs.filter( ':first' ).addClass( 'active' );
			$panels.filter( ':first' ).addClass( 'active' );

			$tabs.on( 'click', function( e ) {
				e.preventDefault();

				var $tab = $( this ),
					index = $tab.parent().index();

				if ( $tab.hasClass( 'active' ) ) {
					return;
				}

				$tabs.removeClass( 'active' );
				$tab.addClass( 'active' );
				$panels.removeClass( 'active' );
				$panels.filter( ':eq(' + index + ')' ).addClass( 'active' );
			} );
		} );
	}

	/* Init tabs */
	$( function() {
		$( '.tabs' ).tabs();
	} );
} )( jQuery );