<?php

/**
 * Define theme shortcodes
 *
 * @package FactoryPlus
 */
class FactoryPlus_Shortcodes {

	/**
	 * Store variables for js
	 *
	 * @var array
	 */
	public $l10n = array();

	public $api_key = '';

	/**
	 * Store variables for maps
	 *
	 * @var array
	 */
	public $maps = array();

	/**
	 * Check if WooCommerce plugin is actived or not
	 *
	 * @var bool
	 */
	private $wc_actived = false;

	/**
	 * Construction
	 *
	 * @return FactoryPlus_Shortcodes
	 */
	function __construct() {

		$this->wc_actived = function_exists( 'is_woocommerce' );

		$shortcodes = array(
			'fp_latest_project',
			'fp_project_carousel',
			'fp_service',
			'fp_service_2',
			'fp_service_3',
			'fp_latest_post',
			'fp_section_title',
			'fp_icon_box',
			'fp_counter',
			'fp_testimonials_grid',
			'fp_testimonials',
			'fp_testimonials_2',
			'fp_testimonials_3',
			'fp_feature_product_carousel',
			'fp_partner',
			'fp_phone',
			'fp_team',
			'fp_faq',
			'fp_accordion',
			'fp_docs',
			'fp_images_list',
			'fp_custom_images',
			'fp_gmap',
		);

		foreach ( $shortcodes as $shortcode ) {
			add_shortcode( $shortcode, array( $this, $shortcode ) );
		}

		add_action( 'wp_enqueue_scripts', array( $this, 'header' ) );
		add_action( 'wp_footer', array( $this, 'footer' ) );

		add_shortcode( 'searchform', array( $this, 'factoryplus_searchform' ) );
	}

	public function footer() {
		// Load Google maps only when needed
		if ( isset( $this->l10n['map'] ) ) {
			echo '<script>if ( typeof google !== "object" || typeof google.maps !== "object" )
				document.write(\'<script src="//maps.google.com/maps/api/js?sensor=false&key=' . $this->api_key . '"><\/script>\')</script>';
		}

		wp_register_script( 'factoryplus-addons-plugins', FACTORYPLUS_ADDONS_URL . '/assets/js/plugins.js', array(), '1.0.0' );
		wp_enqueue_script( 'factoryplus-shortcodes', FACTORYPLUS_ADDONS_URL . '/assets/js/frontend.js', array( 'jquery', 'factoryplus-addons-plugins' ), '1.0.0', true );

		wp_localize_script( 'factoryplus', 'factoryplusShortCode', $this->l10n );
	}

	/**
	 * Load JS in header
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function header() {
		wp_enqueue_style( 'factoryplus-shortcodes', FACTORYPLUS_ADDONS_URL . '/assets/css/frontend.css', array(), '1.0.0' );

		$color_scheme_option = '';
		if ( function_exists( 'factoryplus_get_option' ) ) {
			$color_scheme_option = factoryplus_get_option( 'color_scheme' );

			if ( intval( factoryplus_get_option( 'custom_color_scheme' ) ) ) {
				$color_scheme_option = factoryplus_get_option( 'custom_color' );
			}
		} else {
			$color_scheme_option = get_theme_mod( 'color_scheme', '' );
			if ( intval( get_theme_mod( 'custom_color_scheme', '' ) ) ) {
				$color_scheme_option = get_theme_mod( 'custom_color', '' );
			}
		}

		// Don't do anything if the default color scheme is selected.
		if ( $color_scheme_option ) {
			$css = $this->factoryplus_get_color_scheme_css( $color_scheme_option );
			wp_add_inline_style( 'factoryplus-shortcodes', $css );
		}
	}

	/**
	 * Add shortcode searchform
	 */
	function factoryplus_searchform( $form ) {
		$form = '<form method="get" class="search-form" action="' . esc_url( home_url( '/' ) ) . '">
		<label>
			<span class="screen-reader-text">Search for:</span>
			<input type="search" class="search-field" placeholder="' . esc_html__( 'Search', 'factoruplus' ) . '" value="" name="s">
		</label>
		<input type="submit" class="search-submit" value="Search">
		</form>';

		return $form;
	}

	/**
	 * Shortcode to display section title
	 *
	 * @param  array  $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function fp_section_title( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'title'      => '',
				'style'      => '',
				'light_skin' => false,
				'desc'       => '',
				'el_class'   => '',
			), $atts
		);

		$css_class[] = $atts['el_class'];
		$css_class[] = $atts['style'];

		$desc = '';

		if ( $atts['light_skin'] ) {
			$css_class[] = 'light-skin';
		}

		if ( $atts['style'] == 'style-2' ) {
			$desc = sprintf( '<p>%s</p>', $atts['desc'] );
		}

		return sprintf(
			'<div class="fp-section-title clearfix %s">
                <h2>%s</h2>
                %s
            </div>',
			esc_attr( implode( ' ', $css_class ) ),
			$atts['title'],
			$desc
		);
	}

	/**
	 * Shortcode to display section title
	 *
	 * @param  array  $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function fp_docs( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'title'    => '',
				'desc'     => '',
				'el_class' => '',
			), $atts
		);

		$css_class[] = $atts['el_class'];

		return sprintf(
			'<div class="fp-docs %s">
				<span class="icon_q">Q</span>
                <h2>%s</h2>
                <p>%s</p>
            </div>',
			esc_attr( implode( ' ', $css_class ) ),
			$atts['title'],
			$atts['desc']
		);
	}

	/**
	 * Shortcode to display FAQ
	 *
	 * @param  array  $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function fp_faq( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'title'    => '',
				'the_last' => false,
				'open'     => false,
				'el_class' => '',
			), $atts
		);

		$css_class[] = $atts['el_class'];

		$output = array();

		if ( $atts['the_last'] == true ) {
			$css_class[] = 'the-last-faq';
		}

		if ( $atts['open'] == true ) {
			$css_class[] = 'active';
		}

		if ( $content ) {
			if ( function_exists( 'wpb_js_remove_wpautop' ) ) {
				$content = wpb_js_remove_wpautop( $content, true );
			}
			$output[] = sprintf( '<div class="toggle-content">%s</div>', $content );
		}

		return sprintf(
			'<div class="fp-faq %s">
                <h2>%s<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></h2>
                %s
            </div>',
			esc_attr( implode( ' ', $css_class ) ),
			$atts['title'],
			implode( '', $output )
		);
	}

	/**
	 * Shortcode to display FAQ
	 *
	 * @param  array  $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function fp_accordion( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'title'    => '',
				'the_last' => false,
				'open'     => false,
				'style'    => 'style-1',
				'el_class' => '',
			), $atts
		);

		$css_class[] = $atts['el_class'];

		$css_class[] = $atts['style'];

		$output = array();

		if ( $atts['the_last'] == true ) {
			$css_class[] = 'the-last-faq';
		}

		if ( $atts['open'] == true ) {
			$css_class[] = 'active';
		}

		if ( $content ) {
			if ( function_exists( 'wpb_js_remove_wpautop' ) ) {
				$content = wpb_js_remove_wpautop( $content, true );
			}
			$output[] = sprintf( '<div class="toggle-content">%s</div>', $content );
		}

		return sprintf(
			'<div class="fp-accordion %s">
				<div class="accordion-title">
					<span class="icons">
						<i class="fa fa-plus" aria-hidden="true"></i>
						<i class="fa fa-minus" aria-hidden="true"></i>
					</span>
	                <h2>%s</h2>
				</div>
                %s
            </div>',
			esc_attr( implode( ' ', $css_class ) ),
			$atts['title'],
			implode( '', $output )
		);
	}

	/**
	 * Shortcode to display latest project
	 *
	 * @param  array  $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function fp_latest_project( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'title'         => '',
				'number'        => '8',
				'image_size'    => 'factoryplus-related-project-thumb',
				'overlay'       => '',
				'light_version' => false,
				'el_class'      => '',
			), $atts
		);

		$css_class[] = $atts['el_class'];

		if ( $atts['light_version'] == true ) {
			$css_class[] = 'light-version';
		}

		if ( $atts['image_size'] == 'factoryplus-related-project-thumb' ) {
			$css_class[] = 'style-1';
		} else {
			$css_class[] = 'style-2';
		}

		$overlay = '';

		if ( $atts['overlay'] ) {
			$overlay = 'style="background-color:' . esc_attr( $atts['overlay'] ) . '"';
			//<div class="overlay" %s></div>
		}

		$output = array();

		$query_args = array(
			'posts_per_page'      => $atts['number'],
			'post_type'           => 'project',
			'ignore_sticky_posts' => true,
		);

		$query = new WP_Query( $query_args );

		while ( $query->have_posts() ) : $query->the_post();

			$post_categories = wp_get_post_terms( get_the_ID(), 'project_category' );

			foreach ( $post_categories as $cat ) {
				if ( empty( $cats[$cat->slug] ) ) {
					$cats[$cat->slug] = array( 'name' => $cat->name, 'slug' => $cat->slug, );
				}
			}

			$output[] = sprintf(
				'<div class="col-md-3 col-sm-6 col-xs-6 %s">
					<div class="item-project">
						<a href="%s" class="pro-link"></a>
						<div class="overlay" %s></div>
						%s
						<div class="project-summary">
							<h3 class="project-title"><a href="%s">%s</a></h3>
							<p>%s</p>
						</div>
						<span>%s</span>
					</div>
				</div>',
				implode( ' ', get_post_class() ),
				esc_url( get_the_permalink() ),
				$overlay,
				get_the_post_thumbnail( get_the_ID(), $atts['image_size'] ),
				esc_url( get_the_permalink() ),
				get_the_title(),
				get_the_excerpt(),
				get_post_meta( get_the_ID(), 'project_icon', true )
			);

		endwhile;
		wp_reset_postdata();

		if ( $cats ) {
			$filter = array(
				'<li class="active" data-option-value="*">' . esc_html__( 'View All', 'factoryplus' ) .
				'</li>',
			);
			foreach ( $cats as $category ) {
				$filter[] = sprintf( '<li class="" data-option-value=".project_category-%s">%s</li>', esc_attr( $category['slug'] ), esc_html( $category['name'] ) );
			}

			$filter_html = '<div class="filters-dropdown"><ul class="filter option-set" data-option-key="filter">' . implode( "\n", $filter ) .
				'</ul></div>';

		};

		return sprintf(
			'<div class="fp-latest-project %s">
                <h2>%s</h2>
                %s
                <div class="list-project row">%s</div>
            </div>',
			esc_attr( implode( ' ', $css_class ) ),
			$atts['title'],
			$filter_html,
			implode( '', $output )
		);
	}

	/**
	 * Shortcode to display latest project
	 *
	 * @param  array  $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function fp_project_carousel( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'title'    => '',
				'number'   => '-1',
				'overlay'  => '',
				'el_class' => '',
			), $atts
		);

		$css_class[] = $atts['el_class'];

		$output = array();

		$overlay = '';

		if ( $atts['overlay'] ) {
			$overlay = 'style="background-color:' . esc_attr( $atts['overlay'] ) . '"';
		}

		$query_args = array(
			'posts_per_page'      => $atts['number'],
			'post_type'           => 'project',
			'ignore_sticky_posts' => true,
		);

		$query = new WP_Query( $query_args );

		while ( $query->have_posts() ) : $query->the_post();

			$cats = get_the_terms( get_the_ID(), 'project_category' );

			$cat_html = '';

			if ( ! is_wp_error( $cats ) && $cats ) {
				$cat_html = sprintf( '<a href="%s" class="cat-project">%s</a>', esc_url( get_term_link( $cats[0], 'project_category' ) ), esc_html( $cats[0]->name ) );
			}

			$output[] = sprintf(
				'<div class="item-project">
					<div class="project-thumb">
						<a href="%s" class="pro-link"></a>
						<div class="overlay" %s></div>
						%s
						<i class="factory-link"></i>
						<span>%s</span>
					</div>
					<h3 class="project-title"><a href="%s">%s</a></h3>
					%s
				</div>',
				esc_url( get_the_permalink() ),
				$overlay,
				get_the_post_thumbnail( get_the_ID(), 'factoryplus-project-carousel-thumb' ),
				get_post_meta( get_the_ID(), 'project_icon', true ),
				esc_url( get_the_permalink() ),
				get_the_title(),
				$cat_html
			);

		endwhile;
		wp_reset_postdata();

		return sprintf(
			'<div class="fp-project-carousel %s">
                <h2>%s</h2>
                <div class="list-project">%s</div>
            </div>',
			esc_attr( implode( ' ', $css_class ) ),
			$atts['title'],
			implode( '', $output )
		);
	}

	/**
	 * Shortcode to display latest post
	 *
	 * @param  array  $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function fp_latest_post( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'title'    => '',
				'number'   => '3',
				'el_class' => '',
			), $atts
		);

		$css_class[] = $atts['el_class'];

		$output = array();

		$query_args = array(
			'posts_per_page'      => $atts['number'],
			'post_type'           => 'post',
			'ignore_sticky_posts' => true,
		);

		$query = new WP_Query( $query_args );

		while ( $query->have_posts() ) : $query->the_post();

			$class = has_post_thumbnail() ? '' : 'no-thumb';

			$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

			$time_string = sprintf(
				$time_string,
				esc_attr( get_the_date( 'c' ) ),
				esc_html( get_the_date() )
			);

			$output[] = sprintf(
				'<div class="col-md-4 col-sm-6 col-xs-6 item-latest-post %s">
					<div class="entry-thumbnail"><a href="%s">%s<i class="factory-link"></i></a></div>
					<div class="entry-header">
						<div class="entry-meta">
							<span class="meta byline">
								%s
								<span class="author vcard"><a class="url fn n" href="%s">%s</a></span>
								<span class="meta posted-on"><small class="">&#124;</small>%s</span>
							</span>
						</div>
						<h2 class="entry-title"><a href="%s">%s</a></h2>
					</div>
					<div class="entry-content">%s</div>
				</div>',
				$class,
				esc_url( get_the_permalink() ),
				get_the_post_thumbnail( get_the_ID(), 'factoryplus-blog-grid-thumb' ),
				esc_html__( 'by', 'factoryplus' ),
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				esc_html__( get_the_author() ),
				$time_string,
				esc_url( get_the_permalink() ),
				get_the_title(),
				get_the_excerpt()
			);

		endwhile;
		wp_reset_postdata();

		return sprintf(
			'<div class="fp-latest-post %s">
                <div class="container"><div class="row">%s</div></div>
            </div>',
			esc_attr( implode( ' ', $css_class ) ),
			implode( '', $output )
		);
	}

	/**
	 * Shortcode to display list image
	 *
	 * @param  array  $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function fp_images_list( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'images'     => '',
				'image_size' => '',
				'el_class'   => '',
			), $atts
		);

		$css_class[] = $atts['el_class'];

		$output = array();

		$images = $atts['images'] ? explode( ',', $atts['images'] ) : '';

		if ( $images ) {
			$i = 0;
			foreach ( $images as $attachment_id ) {
				$image = wp_get_attachment_image_src( $attachment_id, $atts['image_size'] );
				if ( $image ) {
					$output[] = sprintf(
						'<div class="img-item item-%s"><img alt="%s"  src="%s"></div>',
						$i + 1,
						esc_attr( $attachment_id ),
						esc_url( $image[0] )
					);
				}
				$i ++;
			}
		}

		return sprintf(
			'<div class="fp-image img-list clearfix %s">
                %s
            </div>',
			esc_attr( implode( ' ', $css_class ) ),
			implode( '', $output )
		);
	}

	/**
	 * Shortcode to display list image
	 *
	 * @param  array  $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function fp_custom_images( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'type'       => 'grid',
				'columns'    => '3',
				'images'     => '',
				'image_size' => '',
				'autoplay'   => 0,
				'nav'        => '',
				'dot'        => '',
				'el_class'   => '',
			), $atts
		);

		$css_class = array(
			'fp-custom-images clearfix',
			'images-' . $atts['type'],
			'columns-' . $atts['columns'],
			$atts['el_class'],
		);

		$autoplay = intval( $atts['autoplay'] );

		if ( ! $autoplay ) {
			$autoplay = false;
		}

		if ( $atts['nav'] ) {
			$nav = true;
		} else {
			$nav = false;
		}

		if ( $atts['dot'] ) {
			$dot = true;
		} else {
			$dot = false;
		}

		$carousel = false;

		if ( $atts['type'] == 'carousel' ) {
			$carousel = true;
		}

		$id                        = uniqid( 'custom-images-slider-' );
		$this->l10n['images'][$id] = array(
			'nav'         => $nav,
			'is_carousel' => $carousel,
			'dot'         => $dot,
			'autoplay'    => $autoplay,
			'columns'     => $atts['columns']
		);

		$output = array();

		$images = $atts['images'] ? explode( ',', $atts['images'] ) : '';

		if ( $images ) {
			foreach ( $images as $attachment_id ) {
				$item = '';

				if ( function_exists( 'wpb_getImageBySize' ) ) {
					$image = wpb_getImageBySize(
						array(
							'attach_id'  => $attachment_id,
							'thumb_size' => $atts['image_size'],
						)
					);

					$item .= $image['thumbnail'];
				} else {
					$image = wp_get_attachment_image_src( $attachment_id, $atts['image_size'] );
					if ( $image ) {
						$item .= sprintf(
							'<img alt="%s" src="%s">',
							esc_attr( $attachment_id ),
							esc_url( $image[0] )
						);
					}
				}

				if ( $image ) {
					$output[] = sprintf(
						'<div class="img-item">%s</div>',
						$item
					);
				}
			}
		}

		return sprintf(
			'<div class="%s">
                <div class="img-list row" id="%s">%s</div>
            </div>',
			esc_attr( implode( ' ', $css_class ) ),
			esc_attr( $id ),
			implode( '', $output )
		);
	}

	/**
	 * Shortcode to display service
	 *
	 * @param  array  $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function fp_service( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'number'   => '4',
				'overlay'  => '',
				'column'   => '2',
				'category' => '',
				'style'    => 'style-1',
				'el_class' => '',
			), $atts
		);

		$css_class[] = $atts['el_class'];

		$css_class[] = $atts['style'];

		$output = array();

		$overlay = '';

		if ( $atts['overlay'] ) {
			$overlay = 'style="background-color:' . esc_attr( $atts['overlay'] ) . '"';
			//<div class="overlay" %s></div>
		}

		$name = '';

		if ( $atts['category'] ) {
			$name = $atts['category'];
		}

		$query_args = array(
			'posts_per_page'      => $atts['number'],
			'post_type'           => 'service',
			'ignore_sticky_posts' => true,
			'service_category'    => $name,
		);

		$query = new WP_Query( $query_args );

		while ( $query->have_posts() ) : $query->the_post();

			$col = 12 / intval( $atts['column'] );

			$class = has_post_thumbnail() ? '' : 'no-thumb';

			$output[] = sprintf(
				'<div class="col-md-%s col-%s col-sm-6 col-xs-6 item-service %s">
					<div class="entry-thumbnail">
						<div class="overlay" %s></div>
						<a href="%s"></a>
						%s<i class="factory-link"></i><span>%s</span>
					</div>
					<h2 class="entry-title"><a href="%s">%s</a></h2>
					<p>%s</p>
					<a href="%s" class="fp-btn-2nd readmore">%s</a>
				</div>',
				$col,
				$col,
				$class,
				$overlay,
				esc_url( get_the_permalink() ),
				get_the_post_thumbnail( get_the_ID(), 'factoryplus-service-grid-thumb' ),
				get_post_meta( get_the_ID(), 'icon_class', true ),
				esc_url( get_the_permalink() ),
				get_the_title(),
				get_the_excerpt(),
				esc_url( get_the_permalink() ),
				esc_html__( 'Read More', 'factoryplus' )
			);

		endwhile;
		wp_reset_postdata();

		return sprintf(
			'<div class="fp-service %s">
                <div class="row service-list">%s</div>
            </div>',
			esc_attr( implode( ' ', $css_class ) ),
			implode( '', $output )
		);
	}

	/**
	 * Shortcode to display service
	 *
	 * @param  array  $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function fp_service_2( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'number'   => '6',
				'overlay'  => '',
				'category' => '',
				'el_class' => '',
			), $atts
		);

		$css_class[] = $atts['el_class'];

		$output = array();

		$overlay = '';

		if ( $atts['overlay'] ) {
			$overlay = 'style="background-color:' . esc_attr( $atts['overlay'] ) . '"';
			//<div class="overlay" %s></div>
		}

		$name = '';

		if ( $atts['category'] ) {
			$name = $atts['category'];
		}

		$query_args = array(
			'posts_per_page'      => $atts['number'],
			'post_type'           => 'service',
			'ignore_sticky_posts' => true,
			'service_category'    => $name,
		);

		$query = new WP_Query( $query_args );

		while ( $query->have_posts() ) : $query->the_post();

			$class = has_post_thumbnail() ? '' : 'no-thumb';

			$output[] = sprintf(
				'<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="item-service %s">
						<div class="entry-thumbnail">
							<a href="%s">%s</a>
						</div>
						<div class="service-content">
							<div class="overlay" %s></div>
							<h2 class="entry-title"><a href="%s">%s</a></h2>
							<p>%s</p>
							<a href="%s" class="readmore">%s</a>
						</div>
					</div>
				</div>',
				$class,
				esc_url( get_the_permalink() ),
				get_the_post_thumbnail( get_the_ID(), 'factoryplus-service-grid-thumb-2' ),
				$overlay,
				esc_url( get_the_permalink() ),
				get_the_title(),
				get_the_excerpt(),
				esc_url( get_the_permalink() ),
				esc_html__( 'Read More', 'factoryplus' )
			);

		endwhile;
		wp_reset_postdata();

		return sprintf(
			'<div class="fp-service-2 %s">
                <div class="row service-list">%s</div>
            </div>',
			esc_attr( implode( ' ', $css_class ) ),
			implode( '', $output )
		);
	}

	/**
	 * Shortcode to display service
	 *
	 * @param  array  $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function fp_service_3( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'icon'        => '',
				'title'       => '',
				'style'       => 'light',
				'link'        => '',
				'button_text' => esc_html__( 'Know More', 'factoryplus' ),
				'el_class'    => '',
			), $atts
		);

		$css_class[] = $atts['el_class'];

		$css_class[] = $atts['style'];

		$icon = '';
		if ( $atts['icon'] ) {
			$icon = sprintf( ' <span class="fp-icon"><i class="%s"></i></span>', $atts['icon'] );
		}

		return sprintf(
			'<div class="fp-service-3 %s">
				%s
				<h2>%s</h2>
				<a href="%s">%s<i class="fa fa-caret-right" aria-hidden="true"></i></a>
            </div>',
			esc_attr( implode( ' ', $css_class ) ),
			$icon,
			$atts['title'],
			esc_url( $atts['link'] ),
			esc_html( $atts['button_text'] )
		);
	}

	/**
	 * Shortcode to display Icon Box
	 *
	 * @param  array  $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function fp_icon_box( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'title'         => '',
				'style'         => 'icon-box',
				'sub_title'     => '',
				'icon'          => '',
				'icon_position' => 'center',
				'version'       => 'dark',
				'el_class'      => '',
			), $atts
		);

		$css_class = array(
			'fp-' . $atts['style'],
			'icon-' . $atts['icon_position'],
			$atts['version'] . '-version',
			$atts['el_class'],
		);

		$icon   = '';
		$title  = '';
		$output = array();

		if ( $atts['title'] ) {
			$title = sprintf( ' <h4>%s</h4>', $atts['title'] );
		}
		if ( $atts['icon'] ) {
			$icon = sprintf( ' <span class="fp-icon"><i class="%s"></i></span>', $atts['icon'] );
		}

		if ( $content ) {
			if ( function_exists( 'wpb_js_remove_wpautop' ) ) {
				$content = wpb_js_remove_wpautop( $content, true );
			}
			$output[] = sprintf( '<div class="desc">%s</div>', $content );
		}

		return sprintf(
			'<div class="%s">
				%s
                %s
                <div class="sub-title">%s</div>
                %s
            </div>',
			esc_attr( implode( ' ', $css_class ) ),
			$icon,
			$title,
			$atts['sub_title'],
			implode( '', $output )
		);
	}

	/**
	 * Shortcode to display counter
	 *
	 * @param  array  $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function fp_counter( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'title'    => '',
				'icon'     => '',
				'value'    => '',
				'unit'     => '',
				'style'    => 'style-1',
				'el_class' => '',
			), $atts
		);

		$css_class[] = $atts['el_class'];

		$css_class[] = $atts['style'];

		$icon = '';
		if ( $atts['icon'] ) {
			$icon = sprintf( ' <span class="fp-icon"><i class="%s"></i></span>', $atts['icon'] );
		}

		return sprintf(
			'<div class="fp-counter %s">
                <div class="counter">
                	<div class="value">%s</div>
                	<span>%s</span>
                </div>
                <h4>%s</h4>
                %s
            </div>',
			esc_attr( implode( ' ', $css_class ) ),
			$atts['value'],
			$atts['unit'],
			$atts['title'],
			$icon
		);
	}

	/**
	 * Shortcode to display counter
	 *
	 * @param  array  $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function fp_partner( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'images'              => '',
				'image_size'          => 'thumbnail',
				'custom_links'        => '',
				'custom_links_target' => '_self',
				'el_class'            => '',
			), $atts
		);

		$output       = array();
		$custom_links = $atts['custom_links'] ? explode( '<br />', $atts['custom_links'] ) : '';
		$images       = $atts['images'] ? explode( ',', $atts['images'] ) : '';

		if ( $images ) {
			$i = 0;
			foreach ( $images as $attachment_id ) {
				$image = wp_get_attachment_image_src( $attachment_id, $atts['image_size'] );
				if ( $image ) {
					$link = '';
					if ( $custom_links && isset( $custom_links[$i] ) ) {
						$link = 'href="' . esc_url( $custom_links[$i] ) . '"';
					}
					$output[] = sprintf(
						'<div class="partner-item">
							<div class="partner-content"><a %s target="%s" ><img alt="%s"  src="%s"></a></div>
						</div>',
						$link,
						esc_attr( $atts['custom_links_target'] ),
						esc_attr( $attachment_id ),
						esc_url( $image[0] )
					);
				}
				$i ++;
			}
		}

		return sprintf(
			'<div class="fp-partner clearfix %s">
				<div class="list-item">%s</div>
			</div>',
			esc_attr( $atts['el_class'] ),
			implode( '', $output )
		);
	}

	/**
	 * Phone shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function fp_phone( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'link'        => '',
				'button_text' => esc_html__( 'Know More', 'factoryplus' ),
				'phone'       => '',
				'hotline'     => '',
				'el_class'    => '',
			), $atts
		);

		return sprintf(
			'<div class="fp-phone clearfix %s">
				<a href="%s" class="fp-btn">%s</a>
				<div class="hotline">
					<i class="factory-technology"></i>
					<span>%s</span>
					<p>%s</p>
				</div>
			</div>',
			esc_attr( $atts['el_class'] ),
			esc_url( $atts['link'] ),
			esc_html( $atts['button_text'] ),
			esc_html( $atts['hotline'] ),
			esc_html( $atts['phone'] )
		);

	}

	/**
	 * Testimonials shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function fp_testimonials_grid( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'number'   => '-1',
				'el_class' => '',
			), $atts
		);


		$output = array();
		$args   = array(
			'post_type'      => 'testimonial',
			'posts_per_page' => $atts['number'],
		);

		$the_query = new WP_Query( $args );

		while ( $the_query->have_posts() ) :
			$the_query->the_post();

			$job = get_post_meta( get_the_ID(), 'testi_job', true );

			if ( $job ) {
				$job = sprintf( '<span class="testi-job">- %s</span>', $job );
			}

			$output[] = sprintf(
				'
				<div class="testi-item col-md-4">
					<div class="testi-des">%s<i class="factory-text"></i></div>
					<div class="testi-info clearfix">
                        %s
                        <h4 class="testi-name">%s</h4>
                        %s
					</div>
                </div>',
				get_the_excerpt(),
				get_the_post_thumbnail( get_the_ID(), 'factoryplus-testimonial-thumb' ),
				get_the_title(),
				$job
			);
		endwhile;
		wp_reset_postdata();

		return sprintf(
			'<div class="fp-testimonials-grid %s">
				<div class="testi-list row">%s</div>
			</div>',
			$atts['el_class'],
			implode( '', $output )
		);
	}

	/**
	 * Testimonials shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function fp_testimonials( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'number'   => '-1',
				'title'    => '',
				'autoplay' => '5000',
				'el_class' => '',
			), $atts
		);

		$autoplay = intval( $atts['autoplay'] );

		if ( ! $autoplay ) {
			$autoplay = false;
		}

		$id                             = uniqid( 'testimonial-slider-' );
		$this->l10n['testimonial'][$id] = array(
			'autoplay' => $autoplay,
		);

		$output = array();
		$args   = array(
			'post_type'      => 'testimonial',
			'posts_per_page' => $atts['number'],
		);

		$the_query = new WP_Query( $args );
		while ( $the_query->have_posts() ) :
			$the_query->the_post();
			$star = get_post_meta( get_the_ID(), 'testi_star', true );

			$stars_html   = array();
			$stars_html[] = '<div class="testi-star">';
			$star         = explode( '.', $star );
			$num          = intval( $star[0] );
			if ( $num ) {
				if ( $num > 0 ) {
					for ( $i = 0; $i < $num; $i ++ ) {
						$stars_html[] = '<i class="fa fa-star fa-md"></i>';
					}
				}
			}
			if ( isset( $star[1] ) ) {
				$stars_html[] = '<i class="fa fa-star-half-empty fa-md"></i>';
			} else {
				$num = 5 - $num;
				if ( $num > 0 ) {
					for ( $i = 0; $i < $num; $i ++ ) {
						$stars_html[] = '<i class="fa fa-star-o fa-md"></i>';
					}
				}
			}
			$stars_html[] = '</div>';

			$job = get_post_meta( get_the_ID(), 'testi_job', true );

			if ( $job ) {
				$job = sprintf( '<span class="testi-job">%s</span>', $job );
			}

			$output[] = sprintf(
				'
				<div class="testi-item">
					<div class="testi-content">
                        <div class="testi-des">%s</div>
                        <span class="testi-name">- %s, </span>
                        %s
                        %s
					</div>
                </div>',
				get_the_excerpt(),
				get_the_title(),
				$job,
				implode( '', $stars_html )
			);
		endwhile;
		wp_reset_postdata();

		return sprintf(
			'<div class="fp-testimonials %s">
				<i class="factory-text"></i>
                <h3>%s</h3>
				<div class="testi-list" id="%s">%s</div>
			</div>',
			esc_attr( $atts['el_class'] ),
			esc_html( $atts['title'] ),
			esc_attr( $id ),
			implode( '', $output )
		);
	}

	/**
	 * Testimonials shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function fp_testimonials_2( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'number'    => '-1',
				'autoplay'  => '5000',
				'no_margin' => false,
				'style'     => 'style-1',
				'el_class'  => '',
			), $atts
		);

		$css_class[] = $atts['el_class'];
		$css_class[] = $atts['style'];

		if ( true == $atts['no_margin'] ) {
			$css_class[] = 'img-no-margin';
		}

		$autoplay = intval( $atts['autoplay'] );

		if ( ! $autoplay ) {
			$autoplay = false;
		}

		$id                              = uniqid( 'testimonial2-slider-' );
		$this->l10n['testimonial2'][$id] = array(
			'autoplay' => $autoplay,
		);

		$output = array();
		$args   = array(
			'post_type'      => 'testimonial',
			'posts_per_page' => $atts['number'],
		);

		$the_query = new WP_Query( $args );
		while ( $the_query->have_posts() ) :
			$the_query->the_post();

			$job      = get_post_meta( get_the_ID(), 'testi_job', true );
			$feedback = get_post_meta( get_the_ID(), 'feedback_title', true );

			if ( $job ) {
				$job = sprintf( '<span class="testi-job">- %s</span>', $job );
			}

			if ( $feedback ) {
				$feedback = sprintf( '<h2 class="testi-feedback">%s</h2>', $feedback );
			}

			$output[] = sprintf(
				'
				<div class="testi-item">
					<div class="testi-des">
						%s
						%s
						<i class="factory-text"></i>
					</div>
					<div class="testi-info clearfix">
                        %s
                        <h4 class="testi-name">%s</h4>
                        %s
					</div>
                </div>',
				$feedback,
				$atts['style'] == 'style-1' ? get_the_content() : get_the_excerpt(),
				get_the_post_thumbnail( get_the_ID(), 'factoryplus-testimonial-thumb' ),
				get_the_title(),
				$job
			);
		endwhile;
		wp_reset_postdata();

		return sprintf(
			'<div class="fp-testimonials-2 %s">
				<div class="testi-list" id="%s">%s</div>
			</div>',
			esc_attr( implode( ' ', $css_class ) ),
			esc_attr( $id ),
			implode( '', $output )
		);
	}

	/**
	 * Testimonials shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function fp_testimonials_3( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'number'   => '-1',
				'title'    => '',
				'autoplay' => '5000',
				'el_class' => '',
			), $atts
		);

		$autoplay = intval( $atts['autoplay'] );

		if ( ! $autoplay ) {
			$autoplay = false;
		}

		$id                              = uniqid( 'testimonial3-slider-' );
		$this->l10n['testimonial3'][$id] = array(
			'autoplay' => $autoplay,
		);

		$output = array();
		$args   = array(
			'post_type'      => 'testimonial',
			'posts_per_page' => $atts['number'],
		);

		$the_query = new WP_Query( $args );
		while ( $the_query->have_posts() ) :
			$the_query->the_post();

			$job = get_post_meta( get_the_ID(), 'testi_job', true );

			if ( $job ) {
				$job = sprintf( '<span class="testi-job">- %s</span>', $job );
			}

			$output[] = sprintf(
				'<div class="testi-item clearfix">
					%s
					<div class="testi-content">
                        <div class="testi-des">%s</div>
                        <span class="testi-name">%s </span>
                        %s
					</div>
                </div>',
				get_the_post_thumbnail( get_the_ID(), 'factoryplus-testimonial-thumb' ),
				get_the_excerpt(),
				get_the_title(),
				$job
			);
		endwhile;
		wp_reset_postdata();

		return sprintf(
			'<div class="fp-testimonials-3 %s">
                <h3>%s</h3>
				<div class="testi-list" id="%s">%s</div>
			</div>',
			esc_attr( $atts['el_class'] ),
			esc_html( $atts['title'] ),
			esc_attr( $id ),
			implode( '', $output )
		);
	}

	function fp_feature_product_carousel( $atts ) {
		if ( ! $this->wc_actived ) {
			return '';
		}

		$atts = shortcode_atts(
			array(
				'number'   => '-1',
				'columns'  => '4',
				'autoplay' => '5000',
				'el_class' => '',
			), $atts
		);

		$css_class[] = $atts['el_class'];

		$autoplay = intval( $atts['autoplay'] );

		if ( ! $autoplay ) {
			$autoplay = false;
		}

		$id                         = uniqid( 'product-carousel-' );
		$this->l10n['product'][$id] = array(
			'autoplay' => $autoplay,
			'columns'  => intval( $atts['columns'] )
		);

		$meta_query  = WC()->query->get_meta_query();
		$tax_query   = WC()->query->get_tax_query();
		$tax_query[] = array(
			'taxonomy' => 'product_visibility',
			'field'    => 'name',
			'terms'    => 'featured',
			'operator' => 'IN',
		);

		$args = array(
			'post_type'           => 'product',
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 1,
			'posts_per_page'      => $atts['number'],
			'meta_query'          => $meta_query,
			'tax_query'           => $tax_query
		);

		return sprintf(
			'<div class="fp-feature-product feature-product-carousel %s" id="%s">
				%s
			</div>',
			esc_attr( implode( ' ', $css_class ) ),
			esc_attr( $id ),
			$this->product_loop( $args, $atts['columns'] )
		);
	}

	// Team
	function fp_team( $atts, $content ) {

		$socials = array(
			'facebook'  => '',
			'twitter'   => '',
			'google'    => '',
			'rss'       => '',
			'pinterest' => '',
			'linkedin'  => '',
			'youtube'   => '',
			'instagram' => '',
		);

		$atts = shortcode_atts(
			array_merge(
				$socials, array(
					'image'      => '',
					'image_size' => 'full',
					'name'       => '',
					'phone'      => '',
					'job'        => '',
					'desc'       => '',
					'overlay'    => '',
					'el_class'   => '',
				)
			), $atts
		);

		$item = '';

		$output = array();

		if ( $atts['image'] ) {
			if ( function_exists( 'wpb_getImageBySize' ) ) {
				$image = wpb_getImageBySize(
					array(
						'attach_id'  => $atts['image'],
						'thumb_size' => $atts['image_size'],
					)
				);

				$item .= $image['thumbnail'];
			} else {
				$image = wp_get_attachment_image_src( $atts['image'], $atts['image_size'] );
				if ( $image ) {
					$item .= sprintf(
						'<img alt="%s" src="%s">',
						esc_attr( $atts['image'] ),
						esc_url( $image[0] )
					);
				}
			}
		}

		$overlay = '';

		if ( $atts['overlay'] ) {
			$overlay = 'style="background-color:' . esc_attr( $atts['overlay'] ) . '"';
		}

		foreach ( $socials as $social => $url ) {
			if ( empty( $atts[$social] ) ) {
				continue;
			}

			$output[] = sprintf(
				'<li class="%s">
					<a href="%s" target="_blank">
						<i class="fa fa-%s"></i>
					</a>
				</li>',
				'google' == $social ? 'googleplus' : $social,
				esc_url( $atts[$social] ),
				'google' == $social ? 'google-plus' : $social
			);
		}

		$phone = '';

		if ( $atts['phone'] ) {
			$phone = sprintf(
				'<div class="number">%s</div>',
				esc_attr( $atts['phone'] )
			);
		}

		return sprintf(
			'<div class="fp-team %s">
				<div class="team-member">
					<div class="overlay" %s></div>
					%s
					<div class="socials">
						<ul class="list-social clearfix">
							%s
						</ul>
					</div>
					<div class="phone"><i class="factory-technology"></i>%s</div>
				</div>
				<div class="info">
					<h4 class="name">%s</h4>
					<span class="job">%s</span>
					%s
				</div>
			</div>',
			esc_attr( $atts['el_class'] ),
			$overlay,
			$item,
			implode( '', $output ),
			$phone,
			esc_attr( $atts['name'] ),
			esc_attr( $atts['job'] ),
			esc_attr( $atts['desc'] )
		);
	}

	/*
	 * GG Maps shortcode
	 */
	/**
	 * @param $atts
	 * @param $content
	 *
	 * @return string
	 */
	function fp_gmap( $atts, $content ) {
		$atts  = shortcode_atts(
			array(
				'api_key'   => '',
				'marker'    => '',
				'map_color' => '#fac012',
				'road_highway_color' => '#f49555',
				'info'      => '',
				'width'     => '',
				'height'    => '500',
				'zoom'      => '13',
				'el_class'  => '',
			), $atts
		);
		$class = array(
			'fp-map-shortcode',
			$atts['el_class'],
		);

		$style = '';
		if ( $atts['width'] ) {
			$unit = 'px';
			if ( strpos( $atts['width'], '%' ) ) {
				$unit = '%';
			}

			$atts['width'] = intval( $atts['width'] );
			$style .= 'width: ' . $atts['width'] . $unit;
		}
		if ( $atts['height'] ) {
			$unit = 'px';
			if ( strpos( $atts['height'], '%' ) ) {
				$unit = '%';
			}

			$atts['height'] = intval( $atts['height'] );
			$style .= ';height: ' . $atts['height'] . $unit;
		}
		if ( $atts['zoom'] ) {
			$atts['zoom'] = intval( $atts['zoom'] );
		}

		$id   = uniqid( 'fp_map_' );
		$html = sprintf(
			'<div class="%s"><div id="%s" class="fp-map" style="%s"></div></div>',
			implode( ' ', $class ),
			$id,
			$style
		);

		$lats    = array();
		$lng     = array();
		$info    = array();
		$i       = 0;
		$fh_info = function_exists( 'vc_param_group_parse_atts' ) ? vc_param_group_parse_atts( $atts['info'] ) : array();

		if ( ! empty( $fh_info ) ) {
			foreach ( $fh_info as $key => $value ) {
				$map_img = $map_info = $map_html = '';

				if ( isset( $value['image'] ) && $value['image'] ) {
					$map_img = wp_get_attachment_image( $value['image'], 'thumbnail' );
				}

				if ( isset( $value['details'] ) && $value['details'] ) {
					$map_info = sprintf( '<div class="fp-map-info">%s</div>', $value['details'] );
				}

				$map_html = sprintf(
					'<div class="box-wrapper" style="width: 150px">%s<h4>%s</h4>%s</div>',
					$map_img,
					esc_html__( 'Location', 'factoryplus' ),
					$map_info
				);

				$coordinates = $this->get_coordinates( $value['address'], $atts['api_key'] );
				$lats[]      = $coordinates['lat'];
				$lng[]       = $coordinates['lng'];
				$info[]      = $map_html;

				if ( isset( $coordinates['error'] ) ) {
					return $coordinates['error'];
				}

				$i ++;
			}
		}

		$marker = '';
		if ( $atts['marker'] ) {
			if ( filter_var( $atts['marker'], FILTER_VALIDATE_URL ) ) {
				$marker = $atts['marker'];
			} else {
				$attachment_image = wp_get_attachment_image_src( intval( $atts['marker'] ), 'full' );
				$marker           = $attachment_image ? $attachment_image[0] : '';
			}
		}

		$this->api_key = $atts['api_key'];

		$this->l10n['map'][$id] = array(
			'type'               => 'normal',
			'lat'                => $lats,
			'lng'                => $lng,
			'zoom'               => $atts['zoom'],
			'marker'             => $marker,
			'height'             => $atts['height'],
			'info'               => $info,
			'number'             => $i,
			'map_color'          => $atts['map_color'],
			'road_highway_color' => $atts['road_highway_color'],
		);
		return $html;

	}

	/**
	 * Helper function to get coordinates for map
	 *
	 * @since 1.0.0
	 *
	 * @param string $address
	 * @param bool   $refresh
	 *
	 * @return array
	 */
	function get_coordinates( $address, $api_key, $refresh = false ) {
		$address_hash = md5( $address );
		$coordinates  = get_transient( $address_hash );
		$results      = array( 'lat' => '', 'lng' => '' );

		if ( $refresh || $coordinates === false ) {
			$args     = array( 'address' => urlencode( $address ), 'sensor' => 'false', 'key' => $api_key );
			$url      = add_query_arg( $args, 'https://maps.googleapis.com/maps/api/geocode/json' );
			$response = wp_remote_get( $url );

			if ( is_wp_error( $response ) ) {
				$results['error'] = esc_html__( 'Can not connect to Google Maps APIs', 'factoryplus' );

				return $results;
			}

			$data = wp_remote_retrieve_body( $response );

			if ( is_wp_error( $data ) ) {
				$results['error'] = esc_html__( 'Can not connect to Google Maps APIs', 'factoryplus' );

				return $results;
			}

			if ( $response['response']['code'] == 200 ) {
				$data = json_decode( $data );

				if ( $data->status === 'OK' ) {
					$coordinates = $data->results[0]->geometry->location;

					$results['lat']     = $coordinates->lat;
					$results['lng']     = $coordinates->lng;
					$results['address'] = (string) $data->results[0]->formatted_address;

					// cache coordinates for 3 months
					set_transient( $address_hash, $results, 3600 * 24 * 30 * 3 );
				} elseif ( $data->status === 'ZERO_RESULTS' ) {
					$results['error'] = esc_html__( 'No location found for the entered address.', 'factoryplus' );
				} elseif ( $data->status === 'INVALID_REQUEST' ) {
					$results['error'] = esc_html__( 'Invalid request. Did you enter an address?', 'factoryplus' );
				} else {
					$results['error'] = esc_html__( 'Something went wrong while retrieving your map, please ensure you have entered the short code correctly.', 'factoryplus' );
				}
			} else {
				$results['error'] = esc_html__( 'Unable to contact Google API service.', 'factoryplus' );
			}
		} else {
			$results = $coordinates; // return cached results
		}

		return $results;
	}

	/**
	 * Loop over found products.
	 *
	 * @param  array $query_args
	 * @param  int   $columns
	 *
	 * @return string
	 */
	function product_loop( $query_args, $columns ) {
		global $woocommerce_loop;

		$products                    = new WP_Query( $query_args );
		$columns                     = absint( $columns );
		$woocommerce_loop['columns'] = $columns;

		ob_start();

		if ( $products->have_posts() ) {
			woocommerce_product_loop_start();

			while ( $products->have_posts() ) : $products->the_post();
				wc_get_template_part( 'content', 'product' );
			endwhile; // end of the loop.

			woocommerce_product_loop_end();
		}

		woocommerce_reset_loop();
		wp_reset_postdata();

		return '<div class="woocommerce columns-' . $columns . '">' . ob_get_clean() . '</div>';
	}

	/**
	 * Returns CSS for the color schemes.
	 *
	 *
	 * @param array $colors Color scheme colors.
	 *
	 * @return string Color scheme CSS.
	 */
	function factoryplus_get_color_scheme_css( $colors ) {
		return <<<CSS
		/* Color Scheme */

		/*Background Color*/

		.fp-btn,
		.fp-btn-2nd:hover,.fp-btn-2nd:focus,
		.fp-latest-project ul.filter li:after,
		.fp-latest-project ul.filter li.active:after,.fp-latest-project ul.filter li:hover:after,
		.fp-latest-project.light-version ul.filter li:after,
		.fp-latest-project.light-version ul.filter li.active:after,.fp-latest-project.light-version ul.filter li:hover:after,
		.fp-section-title h2:before,
		.fp-section-title h2:after,
		.fp-icon-box:hover .fp-icon,
		.fp-icon-box-6 .fp-icon,
		.fp-counter,
		.fp-testimonials-2 .owl-pagination .owl-page.active span,.fp-testimonials-2 .owl-pagination .owl-page:hover span,
		.fp-testimonials-2.style-2 .owl-pagination .owl-page.active span,.fp-testimonials-2.style-2 .owl-pagination .owl-page:hover span,
		.fp-testimonials-3 .owl-pagination .owl-page.active span,.fp-testimonials-3 .owl-pagination .owl-page:hover span,
		.fp-faq.active h2,
		.fp-faq:hover h2,
		.fp-accordion .icons:hover,
		.fp-accordion.active .icons,
		.fp-accordion.style-1 .icons:hover,
		.fp-accordion.style-1.active .icons,
		.fp-button a:hover,
		.fp-feature-product .owl-controls .owl-buttons div:hover,
		.mejs-container .mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-current,.mejs-container .mejs-controls .mejs-volume-button .mejs-volume-slider .mejs-volume-handle,.mejs-container .mejs-controls .mejs-volume-button .mejs-volume-slider .mejs-volume-current,
		.mejs-container .mejs-controls .mejs-time-rail .mejs-time-current,
		.fp-project-carousel .owl-controls .owl-buttons div:hover{background-color: $colors}

		.fp-button a,
		.fp-testimonials-3 .owl-pagination .owl-page.active span,
		.fp-testimonials-3 .owl-pagination .owl-page:hover span,
		.fp-testimonials-2 .owl-pagination .owl-page.active span,
		.fp-testimonials-2 .owl-pagination .owl-page:hover span,
		.fp-testimonials-2.style-2 .owl-pagination .owl-page.active span,
		.fp-testimonials-2.style-2 .owl-pagination .owl-page:hover span,
		div.main-background-color,
		.fp-testimonials-3 .owl-pagination .owl-page.active span,
		.fp-testimonials-3 .owl-pagination .owl-page:hover span{background-color: $colors !important}


		.fp-service-2 .overlay {background-color: rgba( 0, 0, 0, 0.6 ) !important}

		/*Border Color*/

		.fp-icon-box:hover,
		.fp-testimonials .owl-controls .owl-buttons div:hover,
		.fp-testimonials-2 .owl-pagination .owl-page span,
		.fp-testimonials-3 .owl-pagination .owl-page span,
		.fp-accordion.style-1 .icons,
		.fp-feature-product .owl-controls .owl-buttons div:hover{border-color: $colors}
		/*Border Bottom*/

		.fp-testimonials-3 .testi-item{border-bottom-color: $colors}
		/*Color*/

		.fp-project-carousel .cat-project,
		.fp-latest-post .item-latest-post .entry-thumbnail a i,
		.fp-icon-box .fp-icon,
		.fp-icon-box-2 .sub-title,
		.fp-icon-box-2.light-version .fp-icon,
		.fp-icon-box-3 .fp-icon,
		.fp-icon-box-4 .fp-icon,
		.fp-icon-box-5 .fp-icon,
		.fp-testimonials-grid .testi-job,
		.fp-testimonials i,
		.fp-testimonials .owl-controls .owl-buttons div:hover .fa,
		.fp-testimonials-2 .testi-job,
		.fp-testimonials-3 .testi-job,
		.fp-accordion .icons .fa,
		.fp-accordion.style-1 .icons .fa,
		.fp-phone .hotline span,
		.fp-phone .hotline i,
		.fp-team .team-member ul li:hover a,
		.fp-team .job,
		.mejs-container .mejs-controls .mejs-time span,
		.main-color{color: $colors}

CSS;
	}

}
