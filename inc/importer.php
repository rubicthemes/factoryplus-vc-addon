<?php
/**
 * Hooks for importer
 *
 * @package FactoryPlus
 */


/**
 * Importer the demo content
 *
 * @since  1.0
 *
 */
function factoryplus_vc_addons_importer() {
	return array(
		array(
			'name'       => 'FactoryPlus Default',
			'preview'    => 'http://steelthemes.com/soo-importer/factoryplus/preview.jpg',
			'content'    => 'http://steelthemes.com/soo-importer/factoryplus/demo-content.xml',
			'customizer' => 'http://steelthemes.com/soo-importer/factoryplus/customizer.dat',
			'widgets'    => 'http://steelthemes.com/soo-importer/factoryplus/widgets.wie',
			'sliders'    => 'http://steelthemes.com/soo-importer/factoryplus/sliders.zip',
			'pages'      => array(
				'front_page' => 'Home Page 1',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 270,
					'height' => 270,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 370,
					'height' => 400,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 85,
					'crop'   => 1,
				),
			),
		),

		array(
			'name'       => 'FactoryPlus Homepage 2',
			'preview'    => 'http://steelthemes.com/soo-importer/factoryplus/2/preview.jpg',
			'content'    => 'http://steelthemes.com/soo-importer/factoryplus/demo-content.xml',
			'customizer' => 'http://steelthemes.com/soo-importer/factoryplus/2/customizer.dat',
			'widgets'    => 'http://steelthemes.com/soo-importer/factoryplus/widgets.wie',
			'sliders'    => 'http://steelthemes.com/soo-importer/factoryplus/2/sliders.zip',
			'pages'      => array(
				'front_page' => 'Home Page 2',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 270,
					'height' => 270,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 370,
					'height' => 400,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 85,
					'crop'   => 1,
				),
			),
		),

		array(
			'name'       => 'FactoryPlus Homepage 3',
			'preview'    => 'http://steelthemes.com/soo-importer/factoryplus/3/preview.jpg',
			'content'    => 'http://steelthemes.com/soo-importer/factoryplus/demo-content.xml',
			'customizer' => 'http://steelthemes.com/soo-importer/factoryplus/3/customizer.dat',
			'widgets'    => 'http://steelthemes.com/soo-importer/factoryplus/widgets.wie',
			'sliders'    => 'http://steelthemes.com/soo-importer/factoryplus/3/sliders.zip',
			'pages'      => array(
				'front_page' => 'Home Page 3',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 270,
					'height' => 270,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 370,
					'height' => 400,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 85,
					'crop'   => 1,
				),
			),
		),

		array(
			'name'       => 'FactoryPlus Homepage 4',
			'preview'    => 'http://steelthemes.com/soo-importer/factoryplus/4/preview.jpg',
			'content'    => 'http://steelthemes.com/soo-importer/factoryplus/demo-content.xml',
			'customizer' => 'http://steelthemes.com/soo-importer/factoryplus/4/customizer.dat',
			'widgets'    => 'http://steelthemes.com/soo-importer/factoryplus/widgets.wie',
			'sliders'    => 'http://steelthemes.com/soo-importer/factoryplus/4/sliders.zip',
			'pages'      => array(
				'front_page' => 'Home Page 4',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 270,
					'height' => 270,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 370,
					'height' => 400,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 85,
					'crop'   => 1,
				),
			),
		),

		array(
			'name'       => 'FactoryPlus Homepage 5',
			'preview'    => 'http://steelthemes.com/soo-importer/factoryplus/5/preview.jpg',
			'content'    => 'http://steelthemes.com/soo-importer/factoryplus/demo-content.xml',
			'customizer' => 'http://steelthemes.com/soo-importer/factoryplus/5/customizer.dat',
			'widgets'    => 'http://steelthemes.com/soo-importer/factoryplus/widgets.wie',
			'sliders'    => 'http://steelthemes.com/soo-importer/factoryplus/5/sliders.zip',
			'pages'      => array(
				'front_page' => 'Home Page 5',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 270,
					'height' => 270,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 370,
					'height' => 400,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 85,
					'crop'   => 1,
				),
			),
		),
		array(
			'name'       => 'FactoryPlus Homepage 6',
			'preview'    => 'http://steelthemes.com/soo-importer/factoryplus/6/preview.jpg',
			'content'    => 'http://steelthemes.com/soo-importer/factoryplus/demo-content.xml',
			'customizer' => 'http://steelthemes.com/soo-importer/factoryplus/6/customizer.dat',
			'widgets'    => 'http://steelthemes.com/soo-importer/factoryplus/widgets.wie',
			'sliders'    => 'http://steelthemes.com/soo-importer/factoryplus/6/sliders.zip',
			'pages'      => array(
				'front_page' => 'Home Page 6',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 270,
					'height' => 270,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 370,
					'height' => 400,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 85,
					'crop'   => 1,
				),
			),
		),
		array(
			'name'       => 'FactoryPlus Homepage 7',
			'preview'    => 'http://steelthemes.com/soo-importer/factoryplus/7/preview.jpg',
			'content'    => 'http://steelthemes.com/soo-importer/factoryplus/demo-content.xml',
			'customizer' => 'http://steelthemes.com/soo-importer/factoryplus/7/customizer.dat',
			'widgets'    => 'http://steelthemes.com/soo-importer/factoryplus/widgets.wie',
			'sliders'    => 'http://steelthemes.com/soo-importer/factoryplus/7/sliders.zip',
			'pages'      => array(
				'front_page' => 'Home Page 7',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 270,
					'height' => 270,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 370,
					'height' => 400,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 85,
					'crop'   => 1,
				),
			),
		),
		array(
			'name'       => 'FactoryPlus Homepage 8',
			'preview'    => 'http://steelthemes.com/soo-importer/factoryplus/8/preview.jpg',
			'content'    => 'http://steelthemes.com/soo-importer/factoryplus/demo-content.xml',
			'customizer' => 'http://steelthemes.com/soo-importer/factoryplus/8/customizer.dat',
			'widgets'    => 'http://steelthemes.com/soo-importer/factoryplus/widgets.wie',
			'sliders'    => 'http://steelthemes.com/soo-importer/factoryplus/8/sliders.zip',
			'pages'      => array(
				'front_page' => 'Home Page 8',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 270,
					'height' => 270,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 370,
					'height' => 400,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 85,
					'crop'   => 1,
				),
			),
		),
		array(
			'name'       => 'FactoryPlus Homepage 9',
			'preview'    => 'http://steelthemes.com/soo-importer/factoryplus/9/preview.jpg',
			'content'    => 'http://steelthemes.com/soo-importer/factoryplus/demo-content.xml',
			'customizer' => 'http://steelthemes.com/soo-importer/factoryplus/9/customizer.dat',
			'widgets'    => 'http://steelthemes.com/soo-importer/factoryplus/widgets.wie',
			'sliders'    => 'http://steelthemes.com/soo-importer/factoryplus/9/sliders.zip',
			'pages'      => array(
				'front_page' => 'Home Page 9',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 270,
					'height' => 270,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 370,
					'height' => 400,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 85,
					'crop'   => 1,
				),
			),
		),
	);
}

add_filter( 'soo_demo_packages', 'factoryplus_vc_addons_importer', 20 );
