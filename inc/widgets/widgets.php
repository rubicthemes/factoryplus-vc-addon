<?php
/**
 * Load and register widgets
 *
 * @package FactoryPlus
 */

require_once FACTORYPLUS_ADDONS_DIR . '/inc/widgets/recent-posts.php';
require_once FACTORYPLUS_ADDONS_DIR . '/inc/widgets/tabs.php';
require_once FACTORYPLUS_ADDONS_DIR . '/inc/widgets/popular-posts.php';
require_once FACTORYPLUS_ADDONS_DIR . '/inc/widgets/latest-project.php';
require_once FACTORYPLUS_ADDONS_DIR . '/inc/widgets/service-menu.php';
require_once FACTORYPLUS_ADDONS_DIR . '/inc/widgets/socials.php';

/**
 * Register widgets
 *
 * @since  1.0
 *
 * @return void
 */

if ( ! function_exists( 'factoryplus_register_widgets' ) ) :

	function factoryplus_register_widgets() {
		$widgets = array(
			'FactoryPlus_Recent_Posts_Widget',
			'FactoryPlus_Tabs_Widget',
			'FactoryPlus_PopularPost_Widget',
			'FactoryPlus_Latest_Project_Widget',
			'FactoryPlus_Services_Menu_Widget',
			'FactoryPlus_Social_Links_Widget'
		);

		foreach ( $widgets as $widget ) {
			register_widget( $widget );
		}
	}

	add_action( 'widgets_init', 'factoryplus_register_widgets' );

endif;
