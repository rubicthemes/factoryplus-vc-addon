#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FactoryPlus Visual Composer Addons\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-02-04 10:30+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.2.0; wp-5.0.1"

#: inc/shortcodes.php:403
msgid "View All"
msgstr ""

#: inc/shortcodes.php:568 inc/widgets/popular-posts.php:100
msgid "by"
msgstr ""

#: inc/shortcodes.php:822 inc/shortcodes.php:907
#: inc/widgets/recent-posts.php:28
msgid "Read More"
msgstr ""

#: inc/shortcodes.php:937 inc/shortcodes.php:1139 inc/visual-composer.php:1263
#: inc/visual-composer.php:1422
msgid "Know More"
msgstr ""

#: inc/shortcodes.php:1742
msgid "Location"
msgstr ""

#: inc/shortcodes.php:1808 inc/shortcodes.php:1816
msgid "Can not connect to Google Maps APIs"
msgstr ""

#: inc/shortcodes.php:1834
msgid "No location found for the entered address."
msgstr ""

#: inc/shortcodes.php:1836
msgid "Invalid request. Did you enter an address?"
msgstr ""

#: inc/shortcodes.php:1838
msgid ""
"Something went wrong while retrieving your map, please ensure you have "
"entered the short code correctly."
msgstr ""

#: inc/shortcodes.php:1841
msgid "Unable to contact Google API service."
msgstr ""

#: inc/visual-composer.php:840
msgid "Enable Parallax effect"
msgstr ""

#: inc/visual-composer.php:842 inc/visual-composer.php:850
msgid "Design Options"
msgstr ""

#: inc/visual-composer.php:843
msgid "Enable"
msgstr ""

#: inc/visual-composer.php:844
msgid ""
"Enable this option if you want to have parallax effect on this row. When you "
"enable this option, please set background repeat option as \"Theme "
"defaults\" to make it works."
msgstr ""

#: inc/visual-composer.php:848
msgid "Overlay"
msgstr ""

#: inc/visual-composer.php:852
msgid "Select an overlay color for this row"
msgstr ""

#: inc/visual-composer.php:861
msgid "FactoryPlus Section Title"
msgstr ""

#: inc/visual-composer.php:864 inc/visual-composer.php:918
#: inc/visual-composer.php:975 inc/visual-composer.php:1015
#: inc/visual-composer.php:1047 inc/visual-composer.php:1137
#: inc/visual-composer.php:1198 inc/visual-composer.php:1238
#: inc/visual-composer.php:1293 inc/visual-composer.php:1321
#: inc/visual-composer.php:1410 inc/visual-composer.php:1452
#: inc/visual-composer.php:1498 inc/visual-composer.php:1553
#: inc/visual-composer.php:1584 inc/visual-composer.php:1639
#: inc/visual-composer.php:1665 inc/visual-composer.php:1705
#: inc/visual-composer.php:1757 inc/visual-composer.php:1803
#: inc/visual-composer.php:1839 inc/visual-composer.php:1885
#: inc/visual-composer.php:1993
msgid "FactoryPlus"
msgstr ""

#: inc/visual-composer.php:868 inc/visual-composer.php:922
#: inc/visual-composer.php:979 inc/visual-composer.php:1249
#: inc/visual-composer.php:1376 inc/visual-composer.php:1456
#: inc/visual-composer.php:1502 inc/visual-composer.php:1557
#: inc/visual-composer.php:1608 inc/visual-composer.php:1676
#: inc/visual-composer.php:1775 inc/widgets/latest-project.php:143
#: inc/widgets/popular-posts.php:145 inc/widgets/recent-posts.php:150
#: inc/widgets/service-menu.php:157 inc/widgets/socials.php:103
#: inc/widgets/tabs.php:247 inc/widgets/tabs.php:292 inc/widgets/tabs.php:337
msgid "Title"
msgstr ""

#: inc/visual-composer.php:871 inc/visual-composer.php:925
#: inc/visual-composer.php:982 inc/visual-composer.php:1458
#: inc/visual-composer.php:1504 inc/visual-composer.php:1559
#: inc/visual-composer.php:1679
msgid "Enter the title"
msgstr ""

#: inc/visual-composer.php:876
msgid "Light Skin"
msgstr ""

#: inc/visual-composer.php:878 inc/visual-composer.php:948
#: inc/visual-composer.php:1101 inc/visual-composer.php:1113
#: inc/visual-composer.php:1471 inc/visual-composer.php:1479
#: inc/visual-composer.php:1517 inc/visual-composer.php:1524
#: inc/visual-composer.php:1736 inc/visual-composer.php:1784
msgid "Yes"
msgstr ""

#: inc/visual-composer.php:882 inc/visual-composer.php:1173
#: inc/visual-composer.php:1268 inc/visual-composer.php:1615
msgid "Style"
msgstr ""

#: inc/visual-composer.php:885 inc/visual-composer.php:1337
#: inc/visual-composer.php:1532 inc/visual-composer.php:1618
#: inc/visual-composer.php:1726
msgid "Style 1"
msgstr ""

#: inc/visual-composer.php:886 inc/visual-composer.php:1338
#: inc/visual-composer.php:1533 inc/visual-composer.php:1619
#: inc/visual-composer.php:1727
msgid "Style 2"
msgstr ""

#: inc/visual-composer.php:891
msgid "Description"
msgstr ""

#: inc/visual-composer.php:894
msgid "Enter the description here"
msgstr ""

#: inc/visual-composer.php:903 inc/visual-composer.php:960
#: inc/visual-composer.php:1000 inc/visual-composer.php:1032
#: inc/visual-composer.php:1122 inc/visual-composer.php:1183
#: inc/visual-composer.php:1223 inc/visual-composer.php:1278
#: inc/visual-composer.php:1306 inc/visual-composer.php:1395
#: inc/visual-composer.php:1438 inc/visual-composer.php:1483
#: inc/visual-composer.php:1538 inc/visual-composer.php:1568
#: inc/visual-composer.php:1624 inc/visual-composer.php:1650
#: inc/visual-composer.php:1690 inc/visual-composer.php:1742
#: inc/visual-composer.php:1789 inc/visual-composer.php:1823
#: inc/visual-composer.php:1872 inc/visual-composer.php:1977
#: inc/visual-composer.php:2071
msgid "Extra class name"
msgstr ""

#: inc/visual-composer.php:906 inc/visual-composer.php:963
#: inc/visual-composer.php:1003 inc/visual-composer.php:1035
#: inc/visual-composer.php:1125 inc/visual-composer.php:1186
#: inc/visual-composer.php:1226 inc/visual-composer.php:1281
#: inc/visual-composer.php:1309 inc/visual-composer.php:1398
#: inc/visual-composer.php:1441 inc/visual-composer.php:1487
#: inc/visual-composer.php:1542 inc/visual-composer.php:1572
#: inc/visual-composer.php:1627 inc/visual-composer.php:1653
#: inc/visual-composer.php:1693 inc/visual-composer.php:1745
#: inc/visual-composer.php:1792 inc/visual-composer.php:1827
#: inc/visual-composer.php:1874 inc/visual-composer.php:1981
msgid ""
"If you wish to style particular content element differently, then use this "
"field to add a class name and then refer to it in your css file."
msgstr ""

#: inc/visual-composer.php:915
msgid "FactoryPlus Latest Projects"
msgstr ""

#: inc/visual-composer.php:929 inc/visual-composer.php:986
#: inc/visual-composer.php:1141 inc/visual-composer.php:1202
#: inc/visual-composer.php:1298
msgid "Number of Projects"
msgstr ""

#: inc/visual-composer.php:932 inc/visual-composer.php:989
#: inc/visual-composer.php:1144 inc/visual-composer.php:1205
#: inc/visual-composer.php:1301
msgid "Set numbers of Projects you want to display"
msgstr ""

#: inc/visual-composer.php:936
msgid "Image Size"
msgstr ""

#: inc/visual-composer.php:939
msgid "Small"
msgstr ""

#: inc/visual-composer.php:940
msgid "Medium ( fullwidth )"
msgstr ""

#: inc/visual-composer.php:942
msgid "Select image size"
msgstr ""

#: inc/visual-composer.php:946
msgid "Light version"
msgstr ""

#: inc/visual-composer.php:949
msgid "Switch to light version"
msgstr ""

#: inc/visual-composer.php:953 inc/visual-composer.php:993
#: inc/visual-composer.php:1148 inc/visual-composer.php:1209
#: inc/visual-composer.php:1903
msgid "Background Overlay"
msgstr ""

#: inc/visual-composer.php:956 inc/visual-composer.php:996
#: inc/visual-composer.php:1151 inc/visual-composer.php:1212
#: inc/visual-composer.php:1906
msgid "Select an overlay color for this element"
msgstr ""

#: inc/visual-composer.php:972
msgid "FactoryPlus Projects Carousel"
msgstr ""

#: inc/visual-composer.php:1012
msgid "FactoryPlus Images List"
msgstr ""

#: inc/visual-composer.php:1019 inc/visual-composer.php:1074
#: inc/visual-composer.php:1843 inc/visual-composer.php:1888
msgid "Images"
msgstr ""

#: inc/visual-composer.php:1022 inc/visual-composer.php:1077
#: inc/visual-composer.php:1846 inc/visual-composer.php:1892
msgid "Select images from media library"
msgstr ""

#: inc/visual-composer.php:1026 inc/visual-composer.php:1081
#: inc/visual-composer.php:1850 inc/visual-composer.php:1897
msgid "Image size"
msgstr ""

#: inc/visual-composer.php:1028 inc/visual-composer.php:1083
#: inc/visual-composer.php:1852 inc/visual-composer.php:1899
msgid ""
"Enter image size (Example: \"thumbnail\", \"medium\", \"large\", \"full\" or "
"other sizes defined by theme). Alternatively enter size in pixels (Example: "
"200x100 (Width x Height))."
msgstr ""

#: inc/visual-composer.php:1044
msgid "FactoryPlus Custom Images"
msgstr ""

#: inc/visual-composer.php:1051
msgid "Type"
msgstr ""

#: inc/visual-composer.php:1054
msgid "Grid"
msgstr ""

#: inc/visual-composer.php:1055
msgid "Carousel"
msgstr ""

#: inc/visual-composer.php:1057 inc/visual-composer.php:1069
msgid "Display images in how many columns"
msgstr ""

#: inc/visual-composer.php:1061 inc/visual-composer.php:1155
#: inc/visual-composer.php:1806
msgid "Columns"
msgstr ""

#: inc/visual-composer.php:1064 inc/visual-composer.php:1160
msgid "1 Columns"
msgstr ""

#: inc/visual-composer.php:1065 inc/visual-composer.php:1158
msgid "2 Columns"
msgstr ""

#: inc/visual-composer.php:1066 inc/visual-composer.php:1159
#: inc/visual-composer.php:1811
msgid "3 Columns"
msgstr ""

#: inc/visual-composer.php:1067 inc/visual-composer.php:1810
msgid "4 Columns"
msgstr ""

#: inc/visual-composer.php:1087
msgid "Autoplay"
msgstr ""

#: inc/visual-composer.php:1090
msgid "Set auto play speed in mini-second. Enter 0 to disable auto play."
msgstr ""

#: inc/visual-composer.php:1099
msgid "Show Navigation"
msgstr ""

#: inc/visual-composer.php:1102
msgid "If \"YES\" Enable navigation"
msgstr ""

#: inc/visual-composer.php:1110
msgid "Show Dots"
msgstr ""

#: inc/visual-composer.php:1114
msgid "If \"YES\" Enable dots"
msgstr ""

#: inc/visual-composer.php:1134
msgid "FactoryPlus Service"
msgstr ""

#: inc/visual-composer.php:1162
msgid "Display services in how many columns"
msgstr ""

#: inc/visual-composer.php:1166 inc/visual-composer.php:1216
msgid "Category"
msgstr ""

#: inc/visual-composer.php:1169 inc/visual-composer.php:1219
msgid "Select a category or all categories."
msgstr ""

#: inc/visual-composer.php:1176
msgid "Style 1 ( Hide Button )"
msgstr ""

#: inc/visual-composer.php:1177
msgid "Style 2 ( Show Button )"
msgstr ""

#: inc/visual-composer.php:1179
msgid "Choose 1 style"
msgstr ""

#: inc/visual-composer.php:1195
msgid "FactoryPlus Service 2"
msgstr ""

#: inc/visual-composer.php:1235
msgid "FactoryPlus Service 3"
msgstr ""

#: inc/visual-composer.php:1243 inc/visual-composer.php:1326
#: inc/visual-composer.php:1588
msgid "Icon"
msgstr ""

#: inc/visual-composer.php:1255 inc/visual-composer.php:1414
msgid "Link"
msgstr ""

#: inc/visual-composer.php:1261 inc/visual-composer.php:1420
msgid "Button Text"
msgstr ""

#: inc/visual-composer.php:1271 inc/visual-composer.php:1366
msgid "Light Version"
msgstr ""

#: inc/visual-composer.php:1272 inc/visual-composer.php:1365
msgid "Dark Version"
msgstr ""

#: inc/visual-composer.php:1290
msgid "FactoryPlus Latest Post"
msgstr ""

#: inc/visual-composer.php:1318
msgid "FactoryPlus Icon Box"
msgstr ""

#: inc/visual-composer.php:1333
msgid "Icon Box Style"
msgstr ""

#: inc/visual-composer.php:1339
msgid "Style 3"
msgstr ""

#: inc/visual-composer.php:1340
msgid "Style 4"
msgstr ""

#: inc/visual-composer.php:1341
msgid "Style 5"
msgstr ""

#: inc/visual-composer.php:1342
msgid "Style 6"
msgstr ""

#: inc/visual-composer.php:1344
msgid "Choose 1 style you want to display"
msgstr ""

#: inc/visual-composer.php:1348
msgid "Icon Position"
msgstr ""

#: inc/visual-composer.php:1351
msgid "Center"
msgstr ""

#: inc/visual-composer.php:1352
msgid "Top"
msgstr ""

#: inc/visual-composer.php:1354
msgid "Choose Icon Position"
msgstr ""

#: inc/visual-composer.php:1362
msgid "Version"
msgstr ""

#: inc/visual-composer.php:1368
msgid "Choose Version For Icon Box"
msgstr ""

#: inc/visual-composer.php:1382
msgid "Sub Title"
msgstr ""

#: inc/visual-composer.php:1388 inc/visual-composer.php:1462
#: inc/visual-composer.php:1508
msgid "Content"
msgstr ""

#: inc/visual-composer.php:1391
msgid "Enter the content of this box"
msgstr ""

#: inc/visual-composer.php:1407
msgid "FactoryPlus Phone"
msgstr ""

#: inc/visual-composer.php:1426 inc/visual-composer.php:1914
msgid "Phone"
msgstr ""

#: inc/visual-composer.php:1432
msgid "Hot Line"
msgstr ""

#: inc/visual-composer.php:1450
msgid "FactoryPlus FAQs"
msgstr ""

#: inc/visual-composer.php:1465 inc/visual-composer.php:1511
msgid "Enter the content "
msgstr ""

#: inc/visual-composer.php:1469 inc/visual-composer.php:1515
msgid "Open"
msgstr ""

#: inc/visual-composer.php:1472 inc/visual-composer.php:1518
msgid "Check it if you want toggle to open"
msgstr ""

#: inc/visual-composer.php:1477 inc/visual-composer.php:1522
msgid "Is this the last FAQ"
msgstr ""

#: inc/visual-composer.php:1480 inc/visual-composer.php:1525
msgid "Margin bottom of the last FAQ will be 0"
msgstr ""

#: inc/visual-composer.php:1496
msgid "FactoryPlus Accordion"
msgstr ""

#: inc/visual-composer.php:1529
msgid "Accordion Style"
msgstr ""

#: inc/visual-composer.php:1535
msgid "Choose Accordion Style"
msgstr ""

#: inc/visual-composer.php:1551
msgid "FactoryPlus Documentation"
msgstr ""

#: inc/visual-composer.php:1563 inc/visual-composer.php:1924
msgid "Desc"
msgstr ""

#: inc/visual-composer.php:1565
msgid "Enter the description"
msgstr ""

#: inc/visual-composer.php:1581
msgid "FactoryPlus Counter"
msgstr ""

#: inc/visual-composer.php:1594
msgid "Counter Value"
msgstr ""

#: inc/visual-composer.php:1597
msgid "Input integer value for counting"
msgstr ""

#: inc/visual-composer.php:1601
msgid "Unit"
msgstr ""

#: inc/visual-composer.php:1604
msgid "Enter the Unit. Example: +, % .etc"
msgstr ""

#: inc/visual-composer.php:1611
msgid "Enter the title of this box"
msgstr ""

#: inc/visual-composer.php:1636
msgid "FactoryPlus Testimonial Grid"
msgstr ""

#: inc/visual-composer.php:1643 inc/visual-composer.php:1669
#: inc/visual-composer.php:1709 inc/visual-composer.php:1761
msgid "Number of Testimonial"
msgstr ""

#: inc/visual-composer.php:1646 inc/visual-composer.php:1672
#: inc/visual-composer.php:1712 inc/visual-composer.php:1764
msgid "Set numbers of Testimonial you want to display"
msgstr ""

#: inc/visual-composer.php:1662
msgid "FactoryPlus Testimonial Carousel"
msgstr ""

#: inc/visual-composer.php:1683 inc/visual-composer.php:1716
#: inc/visual-composer.php:1768 inc/visual-composer.php:1782
#: inc/visual-composer.php:1817
msgid "Slider autoplay"
msgstr ""

#: inc/visual-composer.php:1686 inc/visual-composer.php:1719
#: inc/visual-composer.php:1771 inc/visual-composer.php:1820
msgid ""
"Duration of animation between slides (in ms). Enter the value is 0 or empty "
"if you want the slider is not autoplay"
msgstr ""

#: inc/visual-composer.php:1702
msgid "FactoryPlus Testimonial Carousel 2"
msgstr ""

#: inc/visual-composer.php:1723
msgid "Testimonial Style"
msgstr ""

#: inc/visual-composer.php:1729
msgid "Choose Testimonial Style"
msgstr ""

#: inc/visual-composer.php:1733
msgid "Image No Margin"
msgstr ""

#: inc/visual-composer.php:1738
msgid "Image No Margin. Work with Style 1"
msgstr ""

#: inc/visual-composer.php:1754
msgid "FactoryPlus Testimonial Carousel 3"
msgstr ""

#: inc/visual-composer.php:1778
msgid "Enter the Title"
msgstr ""

#: inc/visual-composer.php:1785
msgid "Enables autoplay mode."
msgstr ""

#: inc/visual-composer.php:1801
msgid "FactoryPlus Feature Product Carousel"
msgstr ""

#: inc/visual-composer.php:1813
msgid "Display products in how many columns"
msgstr ""

#: inc/visual-composer.php:1836
msgid "FactoryPlus Partners"
msgstr ""

#: inc/visual-composer.php:1856
msgid "Custom links"
msgstr ""

#: inc/visual-composer.php:1858
msgid "Enter links for each slide here. Divide links with linebreaks (Enter)."
msgstr ""

#: inc/visual-composer.php:1862
msgid "Custom link target"
msgstr ""

#: inc/visual-composer.php:1865
msgid "Same window"
msgstr ""

#: inc/visual-composer.php:1866
msgid "New window"
msgstr ""

#: inc/visual-composer.php:1868
msgid "Select where to open custom links."
msgstr ""

#: inc/visual-composer.php:1883
msgid "Factoryplus Team"
msgstr ""

#: inc/visual-composer.php:1909
msgid "Name"
msgstr ""

#: inc/visual-composer.php:1919
msgid "Job"
msgstr ""

#: inc/visual-composer.php:1930 inc/widgets/socials.php:23
msgid "Facebook"
msgstr ""

#: inc/visual-composer.php:1936 inc/widgets/socials.php:24
msgid "Twitter"
msgstr ""

#: inc/visual-composer.php:1942 inc/widgets/socials.php:25
msgid "Google Plus"
msgstr ""

#: inc/visual-composer.php:1948 inc/widgets/socials.php:35
msgid "RSS"
msgstr ""

#: inc/visual-composer.php:1954 inc/widgets/socials.php:29
msgid "Pinterest"
msgstr ""

#: inc/visual-composer.php:1960 inc/widgets/socials.php:28
msgid "Linkedin"
msgstr ""

#: inc/visual-composer.php:1966
msgid "Youtube"
msgstr ""

#: inc/visual-composer.php:1972 inc/widgets/socials.php:31
msgid "Instagram"
msgstr ""

#: inc/visual-composer.php:1991
msgid "Google Map"
msgstr ""

#: inc/visual-composer.php:1997
msgid "Api Key"
msgstr ""

#: inc/visual-composer.php:2000
#, php-format
msgid "Please go to <a href=\"%s\">Google Maps APIs</a> to get a key"
msgstr ""

#: inc/visual-composer.php:2004
msgid "Marker"
msgstr ""

#: inc/visual-composer.php:2007
msgid "Select an image from media library"
msgstr ""

#: inc/visual-composer.php:2011
msgid "Map Color"
msgstr ""

#: inc/visual-composer.php:2014
msgid "Select color for map"
msgstr ""

#: inc/visual-composer.php:2015 inc/visual-composer.php:2022
msgid "Map Colors"
msgstr ""

#: inc/visual-composer.php:2019
msgid "Road - HighWay Colors"
msgstr ""

#: inc/visual-composer.php:2026
msgid "Address Infomation"
msgstr ""

#: inc/visual-composer.php:2032
msgid "Location Image"
msgstr ""

#: inc/visual-composer.php:2035
msgid "Choose an image from media library"
msgstr ""

#: inc/visual-composer.php:2039
msgid "Details"
msgstr ""

#: inc/visual-composer.php:2044
msgid "Address"
msgstr ""

#: inc/visual-composer.php:2053
msgid "Width(px)"
msgstr ""

#: inc/visual-composer.php:2059
msgid "Height(px)"
msgstr ""

#: inc/visual-composer.php:2065
msgid "Zoom"
msgstr ""

#: inc/visual-composer.php:2073
msgid ""
"If you wish to style particular content element differently, then use this "
"field to add a class name and then refer to it in your css file . "
msgstr ""

#: inc/visual-composer.php:2109
msgid "Quick Search"
msgstr ""

#: inc/visual-composer.php:2124
msgid "All"
msgstr ""

#: inc/widgets/latest-project.php:27
msgid "FactoryPlus - Latest Project"
msgstr ""

#: inc/widgets/latest-project.php:30
msgid "Advanced latest project widget."
msgstr ""

#: inc/widgets/latest-project.php:150 inc/widgets/popular-posts.php:152
#: inc/widgets/recent-posts.php:157 inc/widgets/service-menu.php:168
#: inc/widgets/tabs.php:253 inc/widgets/tabs.php:298
msgid "Number Of Posts"
msgstr ""

#: inc/widgets/latest-project.php:158 inc/widgets/popular-posts.php:165
#: inc/widgets/recent-posts.php:180 inc/widgets/tabs.php:268
#: inc/widgets/tabs.php:313
msgid "Show Thumbnail"
msgstr ""

#: inc/widgets/latest-project.php:162 inc/widgets/popular-posts.php:169
#: inc/widgets/recent-posts.php:184 inc/widgets/tabs.php:273
#: inc/widgets/tabs.php:318
msgid "Thumbnail Size"
msgstr ""

#: inc/widgets/popular-posts.php:29
msgid "FactoryPlus - PopularPost"
msgstr ""

#: inc/widgets/popular-posts.php:32
msgid "Display most popular posts"
msgstr ""

#: inc/widgets/popular-posts.php:160 inc/widgets/recent-posts.php:196
#: inc/widgets/tabs.php:258 inc/widgets/tabs.php:303
msgid "Show Date"
msgstr ""

#: inc/widgets/recent-posts.php:33
msgid "FactoryPlus - Recent Posts"
msgstr ""

#: inc/widgets/recent-posts.php:36
msgid "Advanced recent posts widget."
msgstr ""

#: inc/widgets/recent-posts.php:161
msgid "Select Category: "
msgstr ""

#: inc/widgets/recent-posts.php:163
msgid "All Categories"
msgstr ""

#: inc/widgets/recent-posts.php:201 inc/widgets/tabs.php:263
#: inc/widgets/tabs.php:308
msgid "Show Comment Number"
msgstr ""

#: inc/widgets/recent-posts.php:206
msgid "Show Excerpt"
msgstr ""

#: inc/widgets/recent-posts.php:211
msgid "Excerpt Length (words)"
msgstr ""

#: inc/widgets/recent-posts.php:216
msgid "Show Readmore Text"
msgstr ""

#: inc/widgets/recent-posts.php:220
msgid "Readmore Text:"
msgstr ""

#: inc/widgets/service-menu.php:23 inc/widgets/service-menu.php:69
#: inc/widgets/service-menu.php:72
msgid "See All Services"
msgstr ""

#: inc/widgets/service-menu.php:28
msgid "FactoryPlus - Services Menu"
msgstr ""

#: inc/widgets/service-menu.php:31
msgid "Advanced services menu widget."
msgstr ""

#: inc/widgets/service-menu.php:162
msgid "Category Title"
msgstr ""

#: inc/widgets/service-menu.php:173
msgid "Show Services by Categories"
msgstr ""

#: inc/widgets/service-menu.php:177
msgid "Order By"
msgstr ""

#: inc/widgets/service-menu.php:186
msgid "Order"
msgstr ""

#: inc/widgets/service-menu.php:204
msgid "date"
msgstr ""

#: inc/widgets/service-menu.php:205
msgid "ID"
msgstr ""

#: inc/widgets/service-menu.php:206
msgid "name"
msgstr ""

#: inc/widgets/service-menu.php:219
msgid "DESC"
msgstr ""

#: inc/widgets/service-menu.php:220
msgid "ASC"
msgstr ""

#: inc/widgets/socials.php:26
msgid "Tumblr"
msgstr ""

#: inc/widgets/socials.php:27
msgid "Skype"
msgstr ""

#: inc/widgets/socials.php:30
msgid "Flickr"
msgstr ""

#: inc/widgets/socials.php:32
msgid "Dribbble"
msgstr ""

#: inc/widgets/socials.php:33
msgid "StumbleUpon"
msgstr ""

#: inc/widgets/socials.php:34
msgid "Github"
msgstr ""

#: inc/widgets/socials.php:50
msgid "FactoryPlus - Social Links"
msgstr ""

#: inc/widgets/socials.php:53
msgid "Display links to social media networks."
msgstr ""

#: inc/widgets/socials.php:116
msgid "URL"
msgstr ""

#: inc/widgets/tabs.php:20
msgid "Popular"
msgstr ""

#: inc/widgets/tabs.php:27
msgid "Recent"
msgstr ""

#: inc/widgets/tabs.php:34
msgid "Comments"
msgstr ""

#: inc/widgets/tabs.php:40
msgid "FactoryPlus - Tabs"
msgstr ""

#: inc/widgets/tabs.php:43
msgid ""
"Display most popular posts, recent posts, recent comments in tabbed widget."
msgstr ""

#: inc/widgets/tabs.php:119
#, php-format
msgid "Permalink to %s"
msgstr ""

#: inc/widgets/tabs.php:126
#, php-format
msgid "%s Comments"
msgstr ""

#: inc/widgets/tabs.php:239
msgid "Popular Posts"
msgstr ""

#: inc/widgets/tabs.php:243
msgid "Show Popular Tab"
msgstr ""

#: inc/widgets/tabs.php:284
msgid "Recent Posts"
msgstr ""

#: inc/widgets/tabs.php:288 inc/widgets/tabs.php:333
msgid "Show Recent Posts Tab"
msgstr ""

#: inc/widgets/tabs.php:329
msgid "Recent Comments"
msgstr ""

#: inc/widgets/tabs.php:343
msgid "Number of comments to show"
msgstr ""

#. Name of the plugin
msgid "FactoryPlus Visual Composer Addons"
msgstr ""

#. Description of the plugin
msgid "Extra elements for Visual Composer. It was built for FactoryPlus theme."
msgstr ""

#. URI of the plugin
msgid "http://demo2.steelthemes.com/plugins/factoryplus-vc-addon.zip"
msgstr ""

#. Author of the plugin
msgid "SteelThemes"
msgstr ""

#. Author URI of the plugin
msgid "http://steelthemes.com"
msgstr ""
