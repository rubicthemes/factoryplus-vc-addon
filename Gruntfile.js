module.exports = function(grunt) {
	// Project configuration
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		less: {
			core: {
				options: {
					paths: ["assets/css/"]
				},
				files: {
					"assets/css/frontend.css": "assets/css/frontend.less"
				}
			}
		},
		concat: {
			core: {
				src: ['assets/js/plugins/*.js'],
				dest: 'assets/js/plugins.js'
			}
		},

		jshint: {
			options: {
				"boss": true,
				"curly": true,
				"eqeqeq": false,
				"eqnull": true,
				"es3": true,
				"expr": true,
				"immed": true,
				"noarg": true,
				"onevar": true,
				"quotmark": "single",
				"trailing": true,
				"undef": true,
				"unused": true,
				"ignores": ['js/plugins/'],
				"browser": true,

				"globals": {
					"jQuery": false,
					"google": true,
					"YT": true,
					"WOW": true,
					"Sly": true,
					"Chart": true
				}
			},
			core: {
				expand: true,
				cwd: 'assets/js/',
				src: [
					'frontend.js'
				]
			}
		},

		uglify: {
			core: {
				expand: true,
				cwd: 'assets/js/',
				dest: 'assets/js/',
				ext: '.min.js',
				src: [
					'*.js',
					'*.*.js',
					'!*.min.js'
				]
			}
		},

		imagemin: {
			dynamic: {
				files: [{
					expand: true,
					cwd: 'img/',
					src: ['**/*.{png,jpg,gif}'],
					dest: 'img/'
				}]
			}
		},

		watch: {
			jsdev: {
				options: { livereload: true },
				files: ['assets/js/*.js'],
				tasks: ['jshint', 'uglify']
			},
			css: {
				files: ['assets/css/*.less'],
				tasks: ['less']
			},
			core: {
				options: { livereload: true },
				files: ['*.php', '*/*.php', 'parts/*.php', 'inc/*.php', 'inc/*/*.php', '!inc/backend/*.php'],
				tasks: []
			},
			livereload: {
				options: { livereload: true },
				files: ['assets/css/frontend.css']
			}

		}
	});

	grunt.util.linefeed = "\n";

	// Load tasks.
	require('matchdep').filterDev('grunt-*').forEach( grunt.loadNpmTasks );

	// Register makepot task

	// Register default tasks
	grunt.registerTask('default', ['concat', 'less', 'jshint', 'uglify', 'imagemin', 'watch']);
};
