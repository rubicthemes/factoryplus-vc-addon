<?php
/**
 * Plugin Name: FactoryPlus Visual Composer Addons
 * Plugin URI: http://demo2.steelthemes.com/plugins/factoryplus-vc-addon.zip
 * Description: Extra elements for Visual Composer. It was built for FactoryPlus theme.
 * Version: 1.1.1
 * Author: SteelThemes
 * Author URI: http://steelthemes.com
 * License: GPL2+
 * Text Domain: factoryplus
 * Domain Path: /lang/
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! defined( 'FACTORYPLUS_ADDONS_DIR' ) ) {
	define( 'FACTORYPLUS_ADDONS_DIR', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'FACTORYPLUS_ADDONS_URL' ) ) {
	define( 'FACTORYPLUS_ADDONS_URL', plugin_dir_url( __FILE__ ) );
}

require_once FACTORYPLUS_ADDONS_DIR . '/inc/visual-composer.php';
require_once FACTORYPLUS_ADDONS_DIR . '/inc/shortcodes.php';
require_once FACTORYPLUS_ADDONS_DIR . '/inc/widgets/widgets.php';

if( is_admin()) {
	require_once FACTORYPLUS_ADDONS_DIR . '/inc/importer.php';
}

/**
 * Init
 */
function factoryplus_vc_addons_init() {
	load_plugin_textdomain( 'factoryplus', false, dirname( plugin_basename( __FILE__ ) ) . '/lang' );

	new FactoryPlus_VC;
	new FactoryPlus_Shortcodes;
}

add_action( 'after_setup_theme', 'factoryplus_vc_addons_init' );
